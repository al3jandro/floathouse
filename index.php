<?
  if(isset($_POST['contatofloat']) && !empty($_POST['contatofloat']))	{
  	require 'PHPMailer-master/PHPMailerAutoload.php';
  	$erros = "";
  	$emailsite = 'contato@floathouse.com.br';
  	if( empty($erros) ){
  		$phpmail = new PHPMailer();
  		$phpmail->IsSMTP(); // envia por SMTP
  		$phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
 		$phpmail->SMTPSecure = "ssl";
  		$phpmail->Host = "floathouse.com.br"; // SMTP servers
  		$phpmail->Port = "465";
  		$phpmail->Username = "contato@floathouse.com.br"; // SMTP username
  		$phpmail->Password = "z5g[A$&8)Zz{"; // SMTP password
  		$phpmail->IsHTML(true);
  		# $phpmail->From = $_POST['email'];
  		$phpmail->From = $emailsite;
  		$email  = $_POST['email'];
  		$nomer = utf8_encode($_POST['nome']);
  		$phpmail->AddReplyTo($email, $nomer);
  		$phpmail->FromName = utf8_decode($_POST['nome']);
  		$phpmail->AddAddress("reservas@floathouse.com.br"); // para quem vai >:D
  		#$phpmail->addCC($email, $cpf_cnpj);
  		$phpmail->Subject = $_POST['assunto'];
  		$phpmail->Body .= "Nome: ".$_POST['nome']. " " .$_POST['sobre']."<br />"; 
  		$phpmail->Body .= "Email: ".$_POST['email']."<br />";
  		$phpmail->Body .= "Telefone: ".$_POST['phone']."<br />";
  		$phpmail->Body .= "Mensagem: ".$_POST['mensagem']."<br />";
  		$send = $phpmail->Send();
  		if($send){
  			echo '<script> alert("Sua mensagem foi enviada com sucesso!!"); window.location="http://floathouse.com.br"; </script>';
  		}else{
  			echo "Não foi possível enviar a mensagem. Erro: " .$phpmail->ErrorInfo;
  		}
  	}else{
  		echo $erros;
  	}
  	##########################################################################
 }
?>
<!doctype html>
<html lang="pt_BR">

<head>
    <title>The Float House</title>
    <link rel="icon" href="assets/images/favicon.ico">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Style CSS -->
    <link rel="stylesheet" href="./assets/css/style.css">

    <meta name="keywords" content="The Float House, Float, House, Quartos, Quarto, Localização, Acomodações, Rio Tarumã, Rio, Tarumã">
    <meta name="description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://floathouse.com.br/">
    <meta property="og:site_name" content="The Float House">
    <meta property="og:image" content="http://floathouse.com.br/assets/images/floathouse.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
</head>

<body>
    <header class="view">
        <div class="container pt-4">
            <nav class="navbar navbar-expand-lg navbar-light bg-light th p-0 m-0">
                <a class="navbar-brand ml-4 my-2" href="http://floathouse.com.br/">
                    <img src="./assets/images/logo.png" height="35px" alt="The Float House" />
                </a>
                <ul class="navbar-nav flex-row d-md-flex m-0 mobill">
                    <li class="nav-item pt-1 px-2">
                        <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="p-2">
                            <i class="fab fa-facebook-f co-icon mt-2" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                    <li class="nav-item pt-1 px-2">
                        <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="p-2">
                            <i class="fab fa-instagram co-icon mt-2" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                </ul>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item mr-md-3">
                            <a class="nav-link lead" href="reservas.php">Reservas</a>
                        </li>
                        <li class="nav-item mr-md-3">
                            <a class="nav-link lead" href="estrutura.php">Estrutura</a>
                        </li>
                        <li class="nav-item mr-md-4">
                            <a class="nav-link lead" href="#contato">Contato</a>
                        </li>
                    </ul>
                </div>
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex m-0 d-desktop">
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="p-2">
                            <i class="fab fa-facebook-f text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="p-2">
                            <i class="fab fa-instagram text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class=" intro-2">
            <div class="full-bg-img">
                <div class="mask rgba-black-strong">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-12 col-md-12">
                                <h1 class="display-1 th text-white text-center center-title">#sejoganofloat</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="icons">
        <div class="container my-5">
            <div class="row justify-content-center">
                <div class="col-6 col-sm-6 col-lg-2 text-center">
                    <img src="./assets/images/icon-1.png" height="85px" alt="Quartos" />
                    <h5 class="font-weight-bold text-uppercase mt-3">quartos</h5>
                    <p class="text-icons">
                        2 Quartos com cama casal, banheiro com chuveiro elé trico e todos os utensilios necessários!
                    </p>
                </div>
                <div class="col-6 col-sm-6 col-lg-2 text-center">
                    <img src="./assets/images/icon-2.png" height="85px" alt="Quartos" />
                    <h5 class="font-weight-bold text-uppercase mt-3">gerador</h5>
                    <p class="text-icons">
                        Na parte elétrica, contamos com um gerador, ficando o combustível incluso no aluguel.
                    </p>
                </div>
                <div class="col-6 col-sm-6 col-lg-2 text-center">
                    <img src="./assets/images/icon-4.png" height="85px" alt="Quartos" />
                    <h5 class="font-weight-bold text-uppercase mt-3">localização</h5>
                    <p class="text-icons">
                        Estamos localizados no Tarumã e temos transporte independente, facilitando a sua chegada e saída.
                    </p>
                </div>
                <div class="col-6 col-sm-6 col-lg-2 text-center">
                    <img src="./assets/images/icon-5.png" height="85px" alt="Quartos" />
                    <h5 class="font-weight-bold text-uppercase mt-3">acomodações</h5>
                    <p class="text-icons">
                        Variedade de cômodos para maior conforto, além de ser adaptável para o seu evento.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="parallax">
        <div class="parallax">
            <div class=" intro-2">
                <div class="full-bg-img">
                    <div class="mask rgba-black-strong">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-12 col-sm-12 col-md-9">
                                    <h4 class="display-4 th text-white text-center center-float">
                                        The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contato" class="mt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-5 col-sm-5 col-md-4">
                    <img src="./assets/images/float.png" class="img-fluid" alt="">
                </div>
                <div class="col-7 col-sm-7 col-md-4 align-self-center">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item color-float">
                            <i class="fas fa-2x fa-map-marker-alt mr-3"></i>
                            <span class="text-footer">Rio Tarumã, S/N</span>
                        </li>
                        <li class="list-group-item color-float">
                            <i class="fas fa-2x fa-phone-volume mr-3"></i>
                            <span class="text-footer">+55 92 3305.1234</span>
                        </li>
                        <li class="list-group-item color-float">
                            <i class="fas fa-2x fa-envelope mr-3"></i>
                            <span class="text-footer">reservas@floathouse.com.br</span>
                        </li>
                        <li class="list-group-item color-float">
                            <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="icon-social">
                                <i class="icon-social fab fa-2x fa-instagram mr-1"></i>
                            </a>
                            <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="icon-social">
                                <i class="icon-social fab fa-2x fa-facebook-f mr-1"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <footer class="view-1 pad-20">
        <div class="container">
            <div class="row justify-content-center py-5">
                <div class="col-md-6">
                    <h1 class="display-4 th font-weigth-bold text-center color-float">fale com a gente</h1>

                    <form class="mt-3" method="post" action="">
                        <div class="form-row">
                            <div class="col" id="contato">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome completo">
                            </div>
                            <div class="col">
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="Telefone">
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="assunto" name="assunto" placeholder="Assunto">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="mensagem" id="mensagem" rows="3"></textarea>
                        </div>
                            <button type="submit" class="btn btn-float btn-lg btn-block">Enviar</button>
							<input name="contatofloat" type="hidden" id="contatofloat" value="1">
                    </form>
                </div>
            </div>
        </div>
         
        <div class="container-fluid text-center py-3 footer-copy mt-3">
            © 2019 The Float House - Todos os direitos reservados.
        </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        //Menu Scroll
        $(document).ready(function() {
            $('a[href^="#"]').on('click', function(e) {
                e.preventDefault();

                var target = this.hash;
                var $target = $(target);

                $('html, body').animate({
                    'scrollTop': $target.offset().top
                }, 1000, 'swing');
            });
        });
    </script>
</body>

</html>