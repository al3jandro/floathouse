<!doctype html>
<html lang="pt_BR">

<head>
    <title>The Float House</title>
    <link rel="icon" href="assets/images/favicon.ico">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Style CSS -->
    <link rel="stylesheet" href="./assets/css/style.css">

    <meta name="keywords" content="The Float House, Float, House, Quartos, Quarto, Localização, Acomodações, Rio Tarumã, Rio, Tarumã">
    <meta name="description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://floathouse.com.br/">
    <meta property="og:site_name" content="The Float House">
    <meta property="og:image" content="http://floathouse.com.br/assets/images/floathouse.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
</head>

<body>
    <header class="res">
        <div class="container pt-4">
            <nav class="navbar navbar-expand-lg navbar-light bg-light th p-0 m-0">
                <a class="navbar-brand ml-4 my-2" href="http://floathouse.com.br/">
                    <img src="./assets/images/logo.png" height="35px" alt="The Float House" />
                </a>
                <ul class="navbar-nav flex-row d-md-flex m-0 mobill">
                    <li class="nav-item px-2">
                        <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="p-2">
                            <i class="fab fa-facebook-f co-icon mt-2" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                    <li class="nav-item px-2">
                        <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="p-2">
                            <i class="fab fa-instagram co-icon mt-2" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                </ul>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item mr-md-3 active">
                            <a class="nav-link lead" href="reservas.php">Reservas</a>
                        </li>
                        <li class="nav-item mr-md-3">
                            <a class="nav-link lead" href="estrutura.php">Estrutura</a>
                        </li>
                        <li class="nav-item mr-md-4">
                            <a class="nav-link lead" href="http://floathouse.com.br/#contato">Contato</a>
                        </li>
                    </ul>
                </div>
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex m-0 d-desktop">
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="p-2">
                            <i class="fab fa-facebook-f text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="p-2">
                            <i class="fab fa-instagram text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class=" intro-2">
            <div class="full-bg-img">
                <div class="mask rgba-black-strong">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-12 col-md-12">
                                <h1 class="display-1 th text-white text-center center-title">
                                    reservas</h1>
                                <h5 class="text-center h2 color-float texto-high">
                                    Para a sua maior comodidade, aceitamos reservas para eventos, festas, aniversários e confraternizações, para que você possa aproveitar um momento incrível ao lado de pessoas especiais. Entre em contato com a gente e tenha uma nova experiência na Amazônia!
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="photo">
        <div class="container my-5">
            <div class="row my-5">
                <div class="col-12 col sm-12 col-md-6">
                    <img src="./assets/images/reserva-1.png" class="img-fluid caja-color" alt="">
                </div>
                <div class="col-12 col sm-12 col-md-6 align-self-center">
                    <h2 class="color-float font-weight-bold ml-md-1 mt-3">VARANDA: </h2>
                    <p class="ml-md-1 color-float h4" style="font-style: italic;">
                        Ambiente único, espaçoso e aconchegante para uma ótima confraternização com os amigos.
                    </p>
                </div>
            </div>
            <div class="row my-5 d-flex box-reverse">
                <div class="col-12 col sm-12 col-md-6 align-self-center">
                    <h2 class="color-float font-weight-bold mr-md-1 text-right mt-3">LOUNGE: </h2>
                    <p class="mr-md-1 color-float h4 text-right" style="font-style: italic;">
                        Um Lounge completo, amplo e com uma estrutura super confortável. Perfeito para apreciar a vista e uma boa conversa.
                    </p>
                </div>
                <div class="col-12 col sm-12 col-md-6">
                    <img src="./assets/images/reserva-2.png" class="img-fluid caja-color-r" alt="">
                </div>
            </div>
            <div class="row my-5">
                <div class="col-12 col sm-12 col-md-6 align-self-center">
                    <img src="./assets/images/reserva-3.png" class="img-fluid caja-color mb-4" alt="">
                    <h2 class="color-float font-weight-bold">ESPAÇO PARA REFEIÇÕES: </h2>
                    <p class="color-float h4 mb-3" style="font-style: italic;">
                        Contamos também com um espaço reservado para refeições. Você pode organizar um lindo almoço ou jantar em um local muito confortável.
                    </p>
                </div>
                <div class="col-12 col sm-12 col-md-6">
                    <img src="./assets/images/reserva-4.png" class="img-fluid caja-color mb-4" alt="">
                    <h2 class="color-float font-weight-bold">CHURRASQUEIRA: </h2>
                    <p class="color-float h4" style="font-style: italic;">
                        Espaço com churrasqueira e bancadas. Ideal para deixar a festa completa.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="parallax-2">
        <div class="parallax-1">
            <div class=" intro-2">
                <div class="full-bg-img">
                    <div class="mask rgba-black-strong">
                        <div class="container pad-20">
                            <div class="row justify-content-around">
                                <div class="col-12 col-sm-12 col-lg-5 text-center mt-4 mb-5">
                                    <h4 class="text-white text-center text-shadow">Segunda-Feira à Quinta-feira</h4>
                                    <p class="display-4 text-uppercase color-second mb-0">
                                        r$<span class="display-2" style="font-weight: 800;">1.500</span>,00
                                    </p>
                                    <h5 class="text-white text-center italic">a DÍARIA (10h as 22h)</h5>
                                    <h1 class="text-white text-center">Até 30 pessoas</h1>
                                    <a name="" id="" class="btn btn-warning text-uppercase text-center" href="#" role="button">
                                        <h4 class="color-float mt-2" style="font-weight: bold;">quero reservar</h4>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-12 col-lg-5 text-center mt-4">
                                    <h4 class="text-white text-center">Sexta à Domingo e Feriados</h4>
                                    <p class="display-4 text-uppercase color-second mb-0">
                                        r$<span class="display-2" style="font-weight: 800;">2.000</span>,00
                                    </p>
                                    <h5 class="text-white text-center italic">a DÍARIA (10h as 22h)</h5>
                                    <h1 class="text-white text-center">Até 30 pessoas</h1>
                                    <a name="" id="" class="btn btn-warning text-uppercase text-center" href="#" role="button">
                                        <h4 class="color-float mt-2" style="font-weight: bold;">quero reservar</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="row justify-content-center mt-3">
                                <div class="col-12 col-sm-12 col-md-10 text-center mt-5">
                                    <h5 class="text-white">
                                        Transporte por pessoa:
                                        <span class="h2 font-weight-bolder">R$ 10,00</span>
                                        <span class="italic">(ida e volta)</span>
                                    </h5>
                                    <h5 class="text-white">
                                        Extra pelo pernoite:
                                        <span class="h2 font-weight-bolder">R$ 500,00</span>
                                        <span class="italic">(mediante disponibilidade)</span>
                                    </h5>
                                    <p style="font-size: .8rem;" class="text-white mb-0">Os valores acima são para até 30 convidados. </p>
                                    <p style="font-size: .8rem;" class="text-white mb-0">
                                        Acima disso é cobrado o valor extra de R$ 30,00 por pessoa, máximo de 50 pessoas.
                                    </p>
                                    <p style="font-size: .8rem;" class="text-white">
                                        Equipe como: churrasqueiro, banda, cozinheiros não entram na contagem, mas deve ser informado com antecedência quem e quantos estarão em serviço
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="cta" class="my-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-8 align-self-center">
                    <h1 class="th display-4 color-float">
                        Faça uma reserva com a gente! Estamos lhe aguardando!
                    </h1>
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/pool.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <footer class="bg-float">
        <div class="container-fluid text-center py-3 footer-copy mt-3">
            © 2019 The Float House - Todos os direitos reservados.
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        //Menu Scroll
        $(document).ready(function() {
            $('a[href^="#"]').on('click', function(e) {
                e.preventDefault();

                var target = this.hash;
                var $target = $(target);

                $('html, body').animate({
                    'scrollTop': $target.offset().top
                }, 1000, 'swing');
            });
        });
    </script>
</body>

</html>