<!doctype html>
<html lang="pt_BR">

<head>
    <title>The Float House</title>
    <link rel="icon" href="assets/images/favicon.ico">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Style CSS -->
    <link rel="stylesheet" href="./assets/css/style.css">

    <meta name="keywords" content="The Float House, Float, House, Quartos, Quarto, Localização, Acomodações, Rio Tarumã, Rio, Tarumã">
    <meta name="description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://floathouse.com.br/">
    <meta property="og:site_name" content="The Float House">
    <meta property="og:image" content="http://floathouse.com.br/assets/images/floathouse.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:description" content="The Float House é um espaço dedicado a grupos fechados, ficando o espaço inteiro para você e seus convidados!">
</head>

<body>
    <header class="est h-75">
        <div class="container pt-4">
            <nav class="navbar navbar-expand-lg navbar-light bg-light th p-0 m-0">
                <a class="navbar-brand ml-4 my-2" href="http://floathouse.com.br/">
                    <img src="./assets/images/logo.png" height="35px" alt="The Float House" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item mr-md-3">
                            <a class="nav-link lead" href="reservas.php">Reservas</a>
                        </li>
                        <li class="nav-item mr-md-3 active">
                            <a class="nav-link lead" href="estrutura.php">Estrutura</a>
                        </li>
                        <li class="nav-item mr-md-4">
                            <a class="nav-link lead" href="http://floathouse.com.br/#contato">Contato</a>
                        </li>
                    </ul>
                </div>
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex m-0">
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.facebook.com/The-Float-House-178815669595999/" target="_blank" class="p-2">
                            <i class="fab fa-facebook-f text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                    <li class="nav-item pt-1 px-2 bg-float">
                        <a href="https://www.instagram.com/thefloathouse_/" target="_blank" class="p-2">
                            <i class="fab fa-instagram text-white mt-3" style="font-size: 1.5rem;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class=" intro-2">
            <div class="full-bg-img">
                <div class="mask rgba-black-strong">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-12 col-md-12">
                                <h1 class="display-1 th text-white text-center center-title">
                                    estrutura</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="photo" class="my-5">
        <div class="container mt-3">
            <div class="row justify-content-center pt-5">
                <div class="col-12 col-sm-12 col-md-8">
                    <h5 class="text-center h3 color-float">
                        Contamos com uma estrutura completa e bem planejada para receber você da forma mais aconchegante possível. Cada local foi pensado na sua satisfação, conforto e comodidade.
                    </h5>
                    <h5 class="text-center color-float mt-5">
                        2 Quartos com cama de casal, Sala de jantar com cozinha integrada + Sala de Televisão, banheiro com chuveiro elétrico, no 2º piso uma sala com 4 sofás-cama e visão 360° para a floresta, na área externa temos 1 piscina com água tratada, 2 churrasqueiras,
                        área gourmet, mesas e sofás, deck molhado, chuveiro externo, banheiro químico externo, geleiras e todos os utensílios necessários.
                    </h5>
                </div>
            </div>
        </div>
    </section>
    <section id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-1.png" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-2.png" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-3.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-4.png" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-5.png" class="img-fluid" alt="">
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/gallery-6.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <section id="cta" class="my-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-8 align-self-center">
                    <h1 class="th display-4 color-float">
                        Faça uma reserva com a gente! Estamos lhe aguardando!
                    </h1>
                </div>
                <div class="col-12 col-sm-12 col-md-4">
                    <img src="./assets/images/pool.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <footer class="bg-float">
        <div class="container-fluid text-center py-3 footer-copy mt-3">
            © 2019 The Float House - Todos os direitos reservados.
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        //Menu Scroll
        $(document).ready(function() {
            $('a[href^="#"]').on('click', function(e) {
                e.preventDefault();

                var target = this.hash;
                var $target = $(target);

                $('html, body').animate({
                    'scrollTop': $target.offset().top
                }, 1000, 'swing');
            });
        });
    </script>
</body>

</html>